resource "aws_instance" "jenkins" {
  ami             = "ami-0b828c1c5ac3f13ee"
  instance_type   = "t2.micro"
  security_groups = [aws_security_group.docker_playground-sg.name]
  key_name        = "infinitykey"
  provisioner "remote-exec" {
    inline = [
      "echo start install docker",
      "sudo apt update",
      "sudo apt install -y apt-transport-https ca-certificates curl software-properties-common",
      "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -",
      "sudo add-apt-repository -y 'deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable'",
      "apt-cache policy docker-ce",
      "sudo apt install -y docker-ce",
      "echo ===========",
      "echo install terraform", 
      "sudo apt install -y software-properties-common gnupg2 curl",
      "curl https://apt.releases.hashicorp.com/gpg | gpg --dearmor > hashicorp.gpg",
      "sudo install -o root -g root -m 644 hashicorp.gpg /etc/apt/trusted.gpg.d/",
      "sudo apt-add-repository -y \"deb [arch=$(dpkg --print-architecture)] https://apt.releases.hashicorp.com $(lsb_release -cs) main\"",
      "sudo apt install terraform",
      "terraform --version",
      "sudo groupadd docker",
      "sudo gpasswd -a ubuntu docker",
      "echo Ready! have fun",
    ]
  }
  connection {
    type        = "ssh"
    host        = self.public_ip
    user        = "ubuntu"
    private_key = file("/home/ubuntu/infinitykey.pem")
  }
  root_block_device {
    delete_on_termination = true
    volume_size = 29
    volume_type = "gp2"
  }
  tags = {
    "Name" = "Docker-playground"
  }
}
