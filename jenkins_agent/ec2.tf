resource "aws_instance" "jenkins_agent" {
  ami             = "ami-0b828c1c5ac3f13ee"
  instance_type   = "t2.micro"
  key_name        = "infinitykey"
  provisioner "remote-exec" {
    inline = [
      "echo Create jenkins agent ",
      "echo install git",
      "sudo apt update -y",
      "sudo apt install -y git",
      "echo install java jdk",
      "sudo apt install -y java-1.8.0-openjdk",
      "echo add user jenkins",
      "sudo useradd -d /var/lib/jenkins jenkins",
      "echo Done",
    ]
  }
  connection {
    type        = "ssh"
    host        = self.public_ip
    user        = "ubuntu"
    private_key = file("/home/ubuntu/infinitykey.pem")
  }
  root_block_device {
    delete_on_termination = true
    volume_size = 29
    volume_type = "gp2"
  }
  tags = {
    "Name" = "Jenkins_agent"
  }
}
