resource "aws_instance" "jenkins" {
  ami             = "ami-0b828c1c5ac3f13ee"
  instance_type   = "t2.medium"
  security_groups = [aws_security_group.minikube-sg.name]
  key_name        = "infinitykey"
  provisioner "remote-exec" {
    inline = [
      "echo start install docker",
      "sudo apt update",
      "sudo apt install -y apt-transport-https ca-certificates curl software-properties-common",
      "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -",
      "sudo add-apt-repository -y 'deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable'",
      "apt-cache policy docker-ce",
      "sudo apt install -y docker-ce",
      "echo ===========",
      "echo install terraform", 
      "sudo apt install -y software-properties-common gnupg2 curl",
      "curl https://apt.releases.hashicorp.com/gpg | gpg --dearmor > hashicorp.gpg",
      "sudo install -o root -g root -m 644 hashicorp.gpg /etc/apt/trusted.gpg.d/",
      "sudo apt-add-repository -y \"deb [arch=$(dpkg --print-architecture)] https://apt.releases.hashicorp.com $(lsb_release -cs) main\"",
      "sudo apt install terraform",
      "terraform --version",
      "echo ===========",
      "echo install kubectl",
      "curl -LO \"https://dl.k8s.io/release/v1.26.1/bin/linux/amd64/kubectl\"", 
      "sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl", 
      "kubectl version --client", 
      "echo ===========",
      "echo add user for minikube host",
#      "sudo useradd chen -p pass",
#      "sudo usermod -aG sudo chen",
#      "sudo groupadd docker",
#      "sudo usermod -aG docker chen",
#      "curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64",
      "echo Done!",
      "curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64",
      "sudo install minikube-linux-amd64 /usr/local/bin/minikube",
      "sudo minikube start --force --driver=docker",

    ]
  }
  connection {
    type        = "ssh"
    host        = self.public_ip
    user        = "ubuntu"
    private_key = file("/home/ubuntu/infinitykey.pem")
  }
  root_block_device {
    delete_on_termination = true
    volume_size = 29
    volume_type = "gp2"
  }
  tags = {
    "Name" = "minikube"
  }
}
